#ifndef STREAM_H
#define STREAM_H

#include <stdlib.h>
#include <stdbool.h>

typedef struct membuf_st {
    size_t size;
    size_t count;
    unsigned char *buf;
	void (*error)(const char *, ...);
	bool xdr;
} *membuf_t;

#define DEFAULT_INITIAL_SIZE 8192
void InitMembuf(membuf_t mb, size_t length, char *buf, void (*error)(const char *, ...));
void FreeMembuf(membuf_t mb);

#define MembufHead(mb) ((char*)(mb)->buf)
#define MembufLength(mb) ((mb)->count)

void SetXDR(membuf_t mb, bool xdr);
int OutFormat(membuf_t mb);
int OutChar(membuf_t mb, int c);
int OutBytes(membuf_t mb, void *buf, size_t length);
int OutInteger(membuf_t mb, int32_t val);
int OutIntegerVec(membuf_t mb, int32_t vals[], size_t len);
int OutReal(membuf_t mb, double val);
int OutRealVec(membuf_t mb, double vals[], size_t len);

int InEof(membuf_t mb);
int InFormat(membuf_t mb);
int InChar(membuf_t mb);
int InBytes(membuf_t mb, void *buf, size_t length);
char *InBytesPtr(membuf_t mb, size_t length);
int InInteger(membuf_t mb, int32_t *val);
int InIntegerVec(membuf_t mb, int32_t vals[], size_t len);
int InReal(membuf_t mb, double *val);
int InRealVec(membuf_t mb, double vals[], size_t len);
#endif
