import rdata
import numpy as np

x = {b'a': np.array([[1, 4],
       [2, 5],
       [3, 6]], dtype=np.int32), b'b': np.array([[[ 1,  5,  9],
        [ 3,  7, 11]],

       [[ 2,  6, 10],
        [ 4,  8, 12]]], dtype=np.int32)}


f = open("a.Rdata", "wb")
a = rdata.serialize(x)
f.write(a)
f.close()
print(x)
print( ' '.join(format(x, '02x') for x in a) )
