#include "stream.h"
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <arpa/inet.h>

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define WORDS_BIGENDIAN
#endif

void default_error_helper(const char *format, ...) {
	va_list args;
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
	abort();
}

#define CHUNK_SIZE 2048
#define INCR 8192
static unsigned char *resize_buffer(membuf_t mb, size_t needed) {
    if( needed < 10000000 ) /* ca 10MB */
		needed = (1+2*needed/INCR) * INCR;
    else
		needed = (size_t)((1+1.2*(double)needed/INCR) * INCR);

	unsigned char *tmp = realloc(mb->buf, needed);
	if(tmp == NULL) {
		free(mb->buf); mb->buf = NULL;
		mb->error("cannot allocate buffer");
	} else {
		mb->buf = tmp;
		mb->size = needed;
	}

	return mb->buf;
}

void InitMembuf(membuf_t mb, size_t length, char *buf, void (*error)(const char *, ...)) {
	mb->count = 0;
    mb->size = length;
	mb->error = (error != NULL) ? error : default_error_helper;

	if( buf == NULL ) {
		buf = malloc(length);
		if( buf == NULL )
			mb->error("cannot allocate buffer");
	}

	mb->buf = (unsigned char *)buf;
	mb->xdr = false;
}

void FreeMembuf(membuf_t mb) {
	if(mb->buf != NULL) {
		unsigned char *buf = mb->buf;
		mb->buf = NULL;
		free(buf);
	}
}

void SetXDR(membuf_t mb, bool xdr) {
	mb->xdr = xdr;
}

int OutFormat(membuf_t mb) {
	if( mb->xdr )
		return OutBytes(mb, "X\n", 2);
	else
		return OutBytes(mb, "B\n", 2);
}

int OutChar(membuf_t mb, int c) {
    if( (mb->count >= mb->size) && (resize_buffer(mb, mb->count + 1) == NULL) )
		return -1;

    mb->buf[mb->count++] = c;
	return 1;
}

int OutBytes(membuf_t mb, void *buf, size_t length) {
    size_t needed = mb->count + length;
    if( (needed > mb->size) && (resize_buffer(mb, needed) == NULL) )
		return -1;

    memcpy(mb->buf + mb->count, buf, length);
    mb->count = needed;
	return length;
}

int OutInteger(membuf_t mb, int32_t val) {
	if( mb->xdr )
		val = (int32_t)htonl((uint32_t)(val));

	return OutBytes(mb, &val, sizeof(int32_t));
}

int OutIntegerVec(membuf_t mb, int32_t vals[], size_t len) {
	if( mb->xdr ) {
		int buf[CHUNK_SIZE];
		size_t done, this;
		for(done = 0; done < len; done += this) {
			this = (CHUNK_SIZE < (len - done)) ? CHUNK_SIZE : (len - done);
			for(size_t i = 0; i < this; i++)
				buf[i] = (int32_t)htonl((uint32_t)(vals[done + i]));

			if( OutBytes(mb, buf, (sizeof(int32_t) * this)) < 0 )
				return -1;
		}
		return sizeof(int32_t)*len;
	} else
		return OutBytes(mb, vals, sizeof(int32_t)*len);
}

double htond(double val) {
	int32_t *lp = (int32_t *)&val;
#ifdef WORDS_BIGENDIAN
	*lp = (int32_t)htonl((uint32_t)(*lp));
	lp++;
	*lp = (int32_t)htonl((uint32_t)(*lp));
#else
	int32_t tmp = (int32_t)htonl((uint32_t)(*lp));
	*lp = (int32_t)htonl((uint32_t)(*(lp + 1)));
	lp++;
	*lp = tmp;
#endif
	return val;
}

double ntohd(double val) {
	int32_t *lp = (int32_t *)&val;
#ifdef WORDS_BIGENDIAN
	*lp = (int32_t)ntohl((uint32_t)(*lp));
	lp++;
	*lp = (int32_t)ntohl((uint32_t)(*lp));
#else
	int32_t tmp = (int32_t)ntohl((uint32_t)(*lp));
	*lp = (int32_t)ntohl((uint32_t)(*(lp + 1)));
	lp++;
	*lp = tmp;
#endif
	return val;
}

int OutReal(membuf_t mb, double val) {
	if( mb->xdr )
		val = htond(val);

	return OutBytes(mb, &val, sizeof(double));
}

int OutRealVec(membuf_t mb, double vals[], size_t len) {
	if( mb->xdr ) {
		double buf[CHUNK_SIZE];
		size_t done, this;
		for(done = 0; done < len; done += this) {
			this = (CHUNK_SIZE < (len - done)) ? CHUNK_SIZE : (len - done);
			for(size_t i = 0; i < this; i++)
				buf[i] = htond(vals[done + i]);

			if( OutBytes(mb, buf, (sizeof(double) * this)) < 0 )
				return -1;
		}
		return sizeof(double)*len;
	} else
		return OutBytes(mb, vals, sizeof(double)*len);
}

int InEof(membuf_t mb) {
    return ( mb->count == mb->size ) ? 0 : -1;
}

int InFormat(membuf_t mb) {
	char buf[2];
	InBytes(mb, buf, 2);

	switch( buf[0] ) {
		case 'B':
			mb->xdr = false;
			break;

		case 'X':
			mb->xdr = true;
			break;

		default:
			mb->error("can only unserialize binary format");
			return -1;
	}

	return 0;
}

int InChar(membuf_t mb) {
    if( mb->count >= mb->size ) {
		mb->error("read error");
		return -1;
	}

    return mb->buf[mb->count++];
}

int InBytes(membuf_t mb, void *buf, size_t length) {
    if( mb->count + length > mb->size) {
		mb->error("read error");
		return -1;
	}

    memcpy(buf, mb->buf + mb->count, length);
    mb->count += length;
	return length;
}

char *InBytesPtr(membuf_t mb, size_t length) {
    if( mb->count + length > mb->size) {
		mb->error("read error");
		return NULL;
	}

    char *buf = (char *)(mb->buf + mb->count);
    mb->count += length;
	return buf;
}

int InInteger(membuf_t mb, int32_t* val) {
	int rv = InBytes(mb, val, sizeof(int32_t));
	if( mb->xdr )
		*val = (int32_t)ntohl((uint32_t)(*val));
	return rv;
}

int InIntegerVec(membuf_t mb, int vals[], size_t len) {
	int rv = InBytes(mb, vals, sizeof(int)*len);
	if( mb->xdr ) {
		for(size_t i = 0; i < len; ++i)
			vals[i] = (int32_t)ntohl((uint32_t)(vals[i]));
	}
	return rv;
}

int InReal(membuf_t mb, double *val) {
	int rv = InBytes(mb, val, sizeof(double));
	if( mb->xdr )
		*val = ntohd(*val);
	return rv;
}

int InRealVec(membuf_t mb, double vals[], size_t len) {
	int rv = InBytes(mb, vals, sizeof(double)*len);
	if( mb->xdr ) {
		for(size_t i = 0; i < len; ++i)
			vals[i] = ntohd(vals[i]);
	}
	return rv;
}

