#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#if PY_MAJOR_VERSION >= 3
  #define MOD_ERROR_VAL NULL
  #define MOD_SUCCESS_VAL(val) val
  #define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
  #define MOD_DEF(ob, name, doc, methods) \
          static struct PyModuleDef moduledef = { \
            PyModuleDef_HEAD_INIT, name, doc, -1, methods, }; \
          ob = PyModule_Create(&moduledef);
#else
  #define MOD_ERROR_VAL
  #define MOD_SUCCESS_VAL(val)
  #define MOD_INIT(name) void init##name(void)
  #define MOD_DEF(ob, name, doc, methods) \
          ob = Py_InitModule3(name, methods, doc);
#endif


#include "stream.h"
#include "sxp.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

void py_error(const char *format, ...) {
	va_list args;
	va_start(args, format);
	PyErr_FormatV(PyExc_RuntimeError, format, args);
	va_end(args);
}

#define IS_OBJECT_BIT_MASK (1 << 8)
#define HAS_ATTR_BIT_MASK (1 << 9)
#define HAS_TAG_BIT_MASK (1 << 10)
#define ENCODE_LEVELS(v) ((v) << 12)
#define DECODE_LEVELS(v) ((v) >> 12)
#define DECODE_TYPE(v) ((v) & 255)

static int PackFlags(int type, int levs, int isobj, int hasattr, int hastag) {
    /* We don't write out bit 5 as from R 2.8.0.
       It is used to indicate if an object is in CHARSXP cache
       - not that it matters to this version of R, but it saves
       checking all previous versions.
       Also make sure the HASHASH bit is not written out.
    */
    int val;
    val = type | ENCODE_LEVELS(levs);
    if(isobj) val |= IS_OBJECT_BIT_MASK;
    if(hasattr) val |= HAS_ATTR_BIT_MASK;
    if(hastag) val |= HAS_TAG_BIT_MASK;
    return val;
}

static void UnpackFlags(int flags, int *ptype, int *plevs,
			int *pisobj, int *phasattr, int *phastag) {
    *ptype = DECODE_TYPE(flags);
    *plevs = DECODE_LEVELS(flags);
    *pisobj = flags & IS_OBJECT_BIT_MASK ? true : false;
    *phasattr = flags & HAS_ATTR_BIT_MASK ? true : false;
    *phastag = flags & HAS_TAG_BIT_MASK ? true : false;
}

#define SHORT_LEN_MAX 2147483647
static int WriteLength(membuf_t mb, size_t len) {
	if( len > SHORT_LEN_MAX ) {
		if( OutInteger(mb, -1)  < 0 )
			return -1;
		if( OutInteger(mb, (int)(len / 4294967296L))  < 0 )
			return -1;
		if( OutInteger(mb, (int)(len % 4294967296L))  < 0 )
			return -1;

		return 0;
	} else {
		return OutInteger(mb, len);
	}
}

static int ReadLength(membuf_t mb, size_t *len) {
	int length;
	if( InInteger(mb, &length)  < 0 )
		return -1;

	if( length == -1 ) {
		unsigned int len1, len2;
		if( InInteger(mb, (int*)&len1) < 0 || InInteger(mb, (int*)&len2) < 0 )
			return -1;

		size_t xlen = len1;
		xlen <<= 32;
		xlen += len2;
		*len = xlen;
	} else {
		*len = length;
	}

	return 0;
}

static int WriteFlags(membuf_t mb, int type, bool hasattr, bool hastag) {
	int flags = PackFlags(type, 0, false, hasattr, hastag);
	return OutInteger(mb, flags);
}

static int WriteArrrayDims(membuf_t mb, int nd, npy_intp dimensions[]) {
	if( WriteFlags(mb, LISTSXP, false, true) < 0 )
		return -1;

	if( OutInteger(mb, 1) < 0 )
		return -1;

	int flags = PackFlags(CHARSXP, 64, false, false, false);
	if( OutInteger(mb, flags) < 0 )
		return -1;

	if( OutInteger(mb, 3) < 0 )
		return -1;

	if( OutBytes(mb, "dim", 3) < 0 )
		return -1;

	if( WriteFlags(mb, INTSXP, false, false) < 0 )
		return -1;

	if( OutInteger(mb, nd) < 0 )
		return -1;

	for(int i = 0; i < nd; ++i)
		if( OutInteger(mb, dimensions[i]) < 0 )
			return -1;

	if( OutInteger(mb, NILVALUE_SXP) < 0 )
		return -1;

	return 0;
}

static int WriteArray(PyArrayObject *array, PyArray_Descr *dtype, membuf_t mb, int sxptype) {
	NpyIter* iter;
    NpyIter_IterNextFunc *iternext;
    char** dataptr;
    npy_intp itemsize, *strideptr, *innersizeptr;

	int nd = PyArray_NDIM(array);
	bool hasattr = nd > 1;
	if( WriteFlags(mb, sxptype, hasattr, false) < 0 )
		return -1;

	size_t len = PyArray_SIZE(array);
	if( WriteLength(mb, len) < 0 )
		return -1;

	iter = NpyIter_New(array, NPY_ITER_READONLY| NPY_ITER_EXTERNAL_LOOP| NPY_ITER_COPY,
							NPY_FORTRANORDER, NPY_SAME_KIND_CASTING, dtype);
    if( iter == NULL )
        return -1;

    iternext = NpyIter_GetIterNext(iter, NULL);
    if( iternext == NULL ) {
        NpyIter_Deallocate(iter);
        return -1;
    }

    dataptr = NpyIter_GetDataPtrArray(iter);
    strideptr = NpyIter_GetInnerStrideArray(iter);
    innersizeptr = NpyIter_GetInnerLoopSizePtr(iter);
	itemsize = dtype->elsize;

	int rv = -1;
	do {
		char* data = *dataptr;
		npy_intp count = *innersizeptr;
		npy_intp stride = *strideptr;

		if( sxptype == REALSXP ) {
			if( stride == itemsize ) {
				if( OutRealVec(mb, (double *)data, count) < 0 )
					goto end;
			} else {
				for(npy_intp i = 0; i < count; i++, data += stride)
					if( OutReal(mb, *((double*)data)) < 0 )
						goto end;
			}
		} else {
			if( stride == itemsize ) {
				if( OutIntegerVec(mb, (int32_t *)data, count) < 0 )
					goto end;
			} else {
				for(npy_intp i = 0; i < count; i++, data += stride)
					if( OutInteger(mb, *((int32_t*)data)) < 0 )
						goto end;
			}

		}
	} while (iternext(iter));

	if( hasattr ) {
		npy_intp *dimensions = PyArray_DIMS(array);
		if( WriteArrrayDims(mb, nd, dimensions) < 0 )
			goto end;
	}

    rv = 0;

end:
    NpyIter_Deallocate(iter);
	return rv;
}

static int WriteItem(membuf_t mb, PyObject *item) {
	if( item == Py_None ) {
		if( OutInteger(mb, NILVALUE_SXP) < 0 )
			return -1;
	} else if( PyDict_Check(item) ) {
		PyObject *key, *value;
		Py_ssize_t pos = 0;

		while( PyDict_Next(item, &pos, &key, &value) ) {
			if( ! PyBytes_Check(key) ) {
				py_error("keys expected to be bytes\n");
				return -1;
			}

			if( WriteFlags(mb, LISTSXP, false, true)  < 0 )
				return -1;

			if( WriteFlags(mb, SYMSXP, false, false)  < 0 )
				return -1;

			WriteItem(mb, key);
			WriteItem(mb, value);
		}

		if( OutInteger(mb, NILVALUE_SXP) < 0 )
			return -1;
	} else if( PyList_Check(item) || PyTuple_Check(item)) {
		if( WriteFlags(mb, VECSXP, false, false)  < 0 )
			return -1;

		size_t size = PySequence_Size(item);
		WriteLength(mb, size);
		for(size_t i=0; i<size; i++) {
			PyObject *element = PySequence_Fast_GET_ITEM(item, i);
			if( WriteItem(mb, element) < 0 )
				return -1;
		}
	} else if( PyByteArray_Check(item) ) {
		if( WriteFlags(mb, RAWSXP, false, false) < 0 )
			return -1;

		size_t len = PyByteArray_GET_SIZE(item);
		char *buf = PyByteArray_AS_STRING(item);
		WriteLength(mb, len);
		OutBytes(mb, buf, len);
	} else if( PyUnicode_Check(item) ) {
		if( WriteFlags(mb, STRSXP, false, false) < 0 )
			return -1;

        PyObject *pyStr = PyUnicode_AsEncodedString(item, "ascii", "Error ~");
		char *buf = PyBytes_AS_STRING(pyStr);
		size_t len = PyBytes_GET_SIZE(pyStr);
		WriteLength(mb, 1);

		if( WriteFlags(mb, CHARSXP, false, false) < 0 )
			return -1;
		OutInteger(mb, len);
		OutBytes(mb, buf, len);

		Py_XDECREF(pyStr);
	} else if( PyBytes_Check(item)) {
		if( WriteFlags(mb, CHARSXP, false, false) < 0 )
			return -1;

		char *buf = PyBytes_AS_STRING(item);
		size_t len = PyBytes_GET_SIZE(item);
		OutInteger(mb, len);
		OutBytes(mb, buf, len);
	} else if( PyFloat_Check(item) ) {
		if( WriteFlags(mb, REALSXP, false, false) < 0 ||
			OutInteger(mb, 1)  < 0 ||
			OutReal(mb, PyFloat_AS_DOUBLE(item)) < 0 )
				return -1;
	} else if( PyLong_Check(item) ) {
		int val = PyLong_AsLong(item);
		if( PyErr_Occurred() )
			return -1;

		if( WriteFlags(mb, INTSXP, false, false) < 0 ||
			OutInteger(mb, 1) < 0 ||
			OutInteger(mb, val) < 0 )
				return -1;
	} else if( PyArray_Check(item) ) {
		PyArrayObject *array = (PyArrayObject *)item;
		int typ = PyArray_TYPE(array);

		if( typ == NPY_DOUBLE || typ == NPY_FLOAT ) {
			PyArray_Descr *dtype = PyArray_DescrFromType(NPY_DOUBLE);
			int rv = WriteArray(array, dtype, mb, REALSXP);
			Py_DECREF(dtype);
			if( rv < 0 )
			   return -1;
		} else {
			PyArray_Descr *dtype = PyArray_DescrFromType(NPY_INT32);
			int rv = WriteArray(array, dtype, mb, (typ == NPY_BOOL) ? LGLSXP : INTSXP);
			Py_DECREF(dtype);
			if( rv < 0 )
			   return -1;
		}

	} else {
		PyErr_SetString(PyExc_TypeError, "bad type\n");
        return -1;
	}

	return 0;
}

#define R_Version(v,p,s) (((v) * 65536) + ((p) * 256) + (s))

static int WriteHeader(membuf_t mb) {
	if( OutFormat(mb)  < 0 )
		return -1;

	if( OutInteger(mb, 2)  < 0 )
		return -1;

	if( OutInteger(mb, R_Version(3,3,0))  < 0 )
		return -1;
	if( OutInteger(mb, R_Version(2,3,0))  < 0 )
		return -1;

	return 0;
}

static PyObject *serialize(PyObject *ignored, PyObject *ob) {
	struct membuf_st mb;
	InitMembuf(&mb, DEFAULT_INITIAL_SIZE, NULL, py_error);
	SetXDR(&mb, true);

	PyObject *result;

	if( WriteHeader(&mb)  < 0 || WriteItem(&mb, ob)  < 0 ) {
		result = Py_None;
	} else {
		result = PyByteArray_FromStringAndSize(
					MembufHead(&mb),
					MembufLength(&mb));
	}

	FreeMembuf(&mb);
	return result;
}

static int ReadHeader(membuf_t mb) {
	if( InFormat(mb)  < 0 )
		return -1;

	int version, writer_version, release_version;
	if( InInteger(mb, &version) < 0 )
		return -1;

    if( InInteger(mb, &writer_version) < 0 )
		return -1;
	if( InInteger(mb, &release_version) < 0 )
		return -1;

	if( version != 2 ) {
		py_error("cannot read workspace version %d", version);
		return -1;
	}

	return 0;
}

static int ReadExpected(membuf_t mb, int expected, const char *error_fmt) {
	int dummy;

	if( InInteger(mb, &dummy) < 0 )
		return -1;

	if( dummy != expected ) {
		py_error(error_fmt, dummy);
		return -1;
	}

	return 0;
}

static int AddRef(PyObject *ref_table, PyObject *s) {
	size_t size = PyList_GET_SIZE(ref_table);
	for(size_t i = 0; i < size; ++i) {
		if( s == PyList_GET_ITEM(ref_table, i) )
			return 0;
	}

	return PyList_Append(ref_table, s);
}

static PyObject *GetRef(PyObject *ref_table, int i) {
	return PyList_GET_ITEM(ref_table, i);
}

static PyObject *ReadItem(PyObject *ref_table, membuf_t mb);
static PyObject *ReadSymbol(PyObject *ref_table, membuf_t mb) {
	PyObject *s = ReadItem(ref_table, mb);
	if( s == NULL )
		return NULL;

	if( AddRef(ref_table, s) < 0 )
		return NULL;
	return s;
}

static PyObject *ReadArrrayDims(PyArrayObject *array, PyObject *ref_table, membuf_t mb) {
	PyObject *attribs = ReadItem(ref_table, mb);
	if( ! PyDict_Check(attribs) ) {
		py_error("expecting attribute dictionary, but something else\n");
		return NULL;
	}

	PyObject *dim_key = PyBytes_FromStringAndSize("dim", 3);
	PyObject *dims = PyDict_GetItem(attribs, dim_key);
	Py_DECREF(dim_key);

	if( dims == NULL || ! PyArray_Check(dims) ) {
		py_error("dim attribute not found or wrong type");
		return NULL;
	}

	PyArrayObject *intp_obj = (PyArrayObject *)PyArray_ContiguousFromObject(dims, NPY_INTP, 1, 1);
	if( intp_obj == NULL ) {
		py_error("dim attribute not found or wrong type");
		return NULL;
	}

	npy_intp nd = PyArray_SIZE(intp_obj);
	npy_intp *dimensions = (npy_intp*)PyArray_DATA(intp_obj);

	PyArray_Dims d = { dimensions, nd };
	return PyArray_Newshape(array, &d, NPY_FORTRANORDER);
}

static PyObject *ReadArray(PyObject *ref_table, membuf_t mb, int numtype, bool hasattr, PyArray_Descr *dtype) {
	NpyIter* iter;
    NpyIter_IterNextFunc *iternext;
    char** dataptr;
    npy_intp itemsize, *strideptr, *innersizeptr;

	size_t len;
	if( ReadLength(mb, &len) < 0 )
		return NULL;

	npy_intp fake_dimensions[1] = { len };
	PyArrayObject *array = (PyArrayObject*)PyArray_SimpleNew(1, fake_dimensions, numtype);

	iter = NpyIter_New(array, NPY_ITER_WRITEONLY| NPY_ITER_EXTERNAL_LOOP| NPY_ITER_UPDATEIFCOPY,
							NPY_FORTRANORDER, NPY_UNSAFE_CASTING, dtype);
    if( iter == NULL )
        return NULL;

    iternext = NpyIter_GetIterNext(iter, NULL);
    if( iternext == NULL ) {
        NpyIter_Deallocate(iter);
        return NULL;
    }

    dataptr = NpyIter_GetDataPtrArray(iter);
    strideptr = NpyIter_GetInnerStrideArray(iter);
    innersizeptr = NpyIter_GetInnerLoopSizePtr(iter);
	itemsize = dtype->elsize;

	PyObject *rv = NULL;
	do {
		char* data = *dataptr;
		npy_intp count = *innersizeptr;
		npy_intp stride = *strideptr;

		if( numtype == NPY_DOUBLE ) {
			if( stride == itemsize ) {
				if( InRealVec(mb, (double *)data, count) < 0 )
					goto end;
			} else {
				for(npy_intp i = 0; i < count; i++, data += stride)
					if( InReal(mb, ((double*)data)) < 0 )
						goto end;
			}
		} else {
			if( stride == itemsize ) {
				if( InIntegerVec(mb, (int32_t *)data, count) < 0 )
					goto end;
			} else {
				for(npy_intp i = 0; i < count; i++, data += stride)
					if( InInteger(mb, ((int32_t*)data)) < 0 )
						goto end;
			}

		}

	} while (iternext(iter));

	if( hasattr ) {
		rv = ReadArrrayDims(array, ref_table, mb);
	} else {
		rv = (PyObject *)array;
	}

end:
    NpyIter_Deallocate(iter);
    return rv;
}

#define UNPACK_REF_INDEX(i) ((i) >> 8)
static PyObject *ReadItem(PyObject *ref_table, membuf_t mb) {
	int flags, type, levs, objf, hasattr, hastag;

	if( InInteger(mb, &flags) < 0 )
		return NULL;

	UnpackFlags(flags, &type, &levs, &objf, &hasattr, &hastag);

	if( type == NILVALUE_SXP ) {
		return Py_None;
	} else if (type == REF_SXP ) {
		int idx = UNPACK_REF_INDEX(flags);
		if( idx == 0 ) {
			if( InInteger(mb, &idx) < 0 )
				return NULL;
		}

		idx = idx - 1;
		return GetRef(ref_table, idx);
	} else if( type == SYMSXP ) {
		return ReadSymbol(ref_table, mb);
	} else if( type == LISTSXP && hastag ) {
		PyObject *list = PyDict_New();
		while( type == LISTSXP ) {
			PyObject *key = ReadItem(ref_table, mb);
			if( key == NULL )
				return NULL;

			PyObject *val = ReadItem(ref_table, mb);
			if( val == NULL )
				return NULL;

			if( PyDict_SetItem(list, key, val) < 0 )
				return NULL;

			if( InInteger(mb, &flags) < 0 )
				return NULL;
			UnpackFlags(flags, &type, &levs, &objf, &hasattr, &hastag);
		}

		if( type != NILVALUE_SXP ) {
			py_error("unexpected list item %0x\n", flags);
			return NULL;
		}

		return list;
	} else if( type == VECSXP ) {
		size_t len;
		if( ReadLength(mb, &len) < 0 )
			return NULL;

		PyObject *list = PyList_New(len);
		for(size_t i = 0; i < len; ++i) {
			PyObject *element = ReadItem(ref_table, mb);
			if( element == NULL || PyList_SET_ITEM(list, i, element) < 0 )
				return NULL;
		}
		return list;
	} else if( type == RAWSXP ) {
		size_t len;
		if( ReadLength(mb, &len) < 0 )
			return NULL;

		return PyByteArray_FromStringAndSize(
					InBytesPtr(mb, len),
					len);
	} else if( type == STRSXP ) {
		size_t len;
		if( ReadLength(mb, &len) < 0 )
			return NULL;

		int expected_flags = PackFlags(CHARSXP, 64, false, false, false);
		if( len == 1 ) {
			if( ReadExpected(mb, expected_flags, "unsupported string element flags %8x") < 0 )
				return NULL;

			int strlength;
			if( InInteger(mb, &strlength) < 0 )
				return NULL;

			return PyUnicode_FromStringAndSize(
						InBytesPtr(mb, strlength),
						strlength);
		} else {
			PyObject *list = PyList_New(len);

			for(size_t i = 0; i < len; ++i) {
				if( ReadExpected(mb, expected_flags, "unsupported string element flags %8x") < 0 )
					return NULL;

				int strlength;
				if( InInteger(mb, &strlength) < 0 )
					return NULL;

				PyObject *element = PyUnicode_FromStringAndSize(
						InBytesPtr(mb, strlength), strlength);
				if( element == NULL || PyList_SET_ITEM(list, i, element) < 0 )
					return NULL;
			}
			return list;
		}
	} else if( type == CHARSXP ) {
		int len;
		if( InInteger(mb, &len) < 0 )
			return NULL;

		return PyBytes_FromStringAndSize(
					InBytesPtr(mb, len),
					len);
	} else if( type == REALSXP ) {
		PyArray_Descr *dtype = PyArray_DescrFromType(NPY_DOUBLE);
		PyObject *result = ReadArray(ref_table, mb, NPY_DOUBLE, hasattr, dtype);
		Py_DECREF(dtype);
		return result;
	} else if( type == LGLSXP || type == INTSXP ) {
		PyArray_Descr *dtype = PyArray_DescrFromType(NPY_INT32);
		PyObject *result = ReadArray(ref_table, mb, (type == LGLSXP) ? NPY_BOOL : NPY_INT32, hasattr, dtype);
		Py_DECREF(dtype);
		return result;
	} else {
		py_error("unsupported type %d (%08x)\n", type, flags);
        return NULL;
	}
}

static PyObject *unserialize(PyObject *ignored, PyObject *ob) {
	size_t len;
	char *buf;
	if( PyByteArray_Check(ob) ) {
		len = PyByteArray_GET_SIZE(ob);
		buf = PyByteArray_AS_STRING(ob);
	} if( PyBytes_Check(ob) ) {
		len = PyBytes_GET_SIZE(ob);
		buf = PyBytes_AS_STRING(ob);
	} else {
		PyErr_SetString(PyExc_TypeError, "Expecting bytearray or bytes\n");
		return Py_None;
	}

	struct membuf_st mb;
	InitMembuf(&mb, len, buf, py_error);

	if( ReadHeader(&mb) < 0 )
		return Py_None;

	PyObject *ref_table = PyList_New(0);
	PyObject *result = ReadItem(ref_table, &mb);
	Py_DECREF(ref_table);

	if( result == NULL )
		return Py_None;
	return result;
}

static struct PyMethodDef module_functions[] = {
  {"serialize", (PyCFunction)serialize, METH_O, "Write data to R."},
  {"unserialize", (PyCFunction)unserialize, METH_O, "Read data from R."},
  {NULL,	 (PyCFunction)NULL, 0, NULL}		/* sentinel */
};

static char module_doc[] = "Read/Write data from R\n\n";

MOD_INIT(rdata) {
    PyObject *m;

    MOD_DEF(m, "rdata", module_doc, module_functions)

    if(m == NULL)
        return MOD_ERROR_VAL;

	import_array();

    return MOD_SUCCESS_VAL(m);

}
