from distutils.core import setup, Extension
import numpy as np

extensions = [
    Extension('rdata', sources = ['rdata.c', 'stream.c'], include_dirs=[np.get_include()])
]

dist = setup(
    name = 'RData',
    version = '1.0',
    author="Tom Haber",
    author_email="tom.haber@uhasselt.be",
    description = 'Read/Write r data',
    ext_modules = extensions,
    requires=['numpy']
)
